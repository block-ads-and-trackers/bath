#!/usr/bin/env bash

hostsfiles="./*"
if [[ "$@" != "" ]]; then
	hostsfiles="$@"
fi

for hostsfile in $hostsfiles; do
	if ! [[ -f "$hostsfile" ]]; then
		echo $hostsfile' is not a file. Skipping...'
		continue
	fi
	if [[ "$(basename $hostsfile)" == "$(basename $0)" ]]; then
		continue
	fi

	cat $hostsfile | ../sanitize.sh | sort -u > $hostsfile'1' && mv $hostsfile'1' $hostsfile
	echo 'Lines written after sanitizing:'$(wc -l $hostsfile)
done
