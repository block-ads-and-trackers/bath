#!/usr/bin/env bash

if ! [[ -w /etc/hosts ]]; then
	echo 'This script needs write access to /etc/hosts - run it as root or change permissions.' >&2
	exit 1
fi
if ! [[ -w /etc ]]; then
	echo 'This script needs write access to /etc - run it as root or change permissions.' >&2
	exit 1
fi

source "$(dirname $0)/recipes"

install_file="$default_recipe"
if [[ "$1" != "" ]]; then
	install_file="$1"
fi

if ! [[ -f "$install_file" ]]; then
	echo 'No such file: '$install_file
	exit 1
fi

which diff >/dev/null 2>&1
if [[ "$?" == "0" ]]; then
	diff -q $install_file /etc/hosts >/dev/null 2>&1
	if [[ "$?" == "0" ]]; then
		echo $install_file' is identical to /etc/hosts. Skipping installation.'
		exit 0
	fi
fi

date=$(date -u +%Y_%m_%d_%H_%M_%S)
backupfile='hosts_'$date'.prev'
mv -v /etc/hosts /etc/$backupfile
cp -v $install_file /etc/hosts
