#!/usr/bin/env bash

can_install=1
if ! [[ -w /etc/hosts ]]; then
	echo 'The install script needs write access to /etc/hosts, so it can not run.' >&2
	echo 'Run this script as root, change permissions or run the install script manually afterwards.' >&2
	echo 'If you run this script as root - this will not affect the permissions of the downloaded files.' >&2
	can_install=0
fi
if ! [[ -w /etc ]]; then
	echo 'The install script needs write access to /etc, so it can not run.' >&2
	echo 'Run this script as root, change permissions or run the install script manually afterwards.' >&2
	echo 'If you run this script as root - this will not affect the permissions of the downloaded files.' >&2
	can_install=0
fi

source "$(dirname $0)/recipes"

sources=""
if [[ "$@" != "" ]]; then
	for src in $@; do
		if [[ -z "${recipes[$src]}" ]]; then
			echo 'No such recipe: '$src
			continue
		fi
		for file in ${recipes[$src]}; do
			sources=$(echo $sources $(sed 's/\/.*//g' <<< $file))
		done
	done
fi

runner_of_this_script=$(id -nu)
owner_of_this_script=$(stat -c '%U' $0)
preserve_permissions="eval "
if [[ $runner_of_this_script != $owner_of_this_script ]]; then
	preserve_permissions="su $owner_of_this_script -c "
fi

$preserve_permissions "./prepare.sh $sources"
$preserve_permissions "./build.sh $@"
if [[ "$can_install" != "0" ]]; then
	./install.sh $1
fi
