#!/usr/bin/env bash
data=""
in_place=""
if [[ "$1" != "" ]]; then
	data="$1"
	in_place="-i"
fi

sed $in_place \
	-e '/^\s*#.*/d' \
	-e 's/#.*//g' \
	-e 's/\(^\s\+\)\|\(\s\+$\)//g' \
	-e 's/\(\(0\|0\.0\.0\.0\|127\.0\.0\.1\)\s\+\)\(.*\)/\3/' \
	-e '/^[^A-Za-z0-9]/d' \
	-e '/.*\s.*/d' \
	-e '/^0*$/d' \
	$data
