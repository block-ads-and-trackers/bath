#!/usr/bin/env bash

while [[ 1 ]]; do
	last_prev=$(ls -1 /etc/hosts_*.prev 2>/dev/null | sort | tail -1)
	if [[ "$last_prev" != "" ]]; then
		date=$(date -u +%Y_%m_%d_%H_%M_%S)
		mv -v /etc/hosts /etc/hosts_${date}.reverted
		mv -v $last_prev /etc/hosts
		break
	else
		reverted="$(ls -1 /etc/hosts*.reverted 2>/dev/null)"
		if [[ "$reverted" == "" ]]; then
			echo "There's no previous hosts file."
			break
		fi
		for rh in $reverted; do
			mv -v $rh $(sed 's/reverted$/prev/g' <<< $rh)
		done
	fi
done
