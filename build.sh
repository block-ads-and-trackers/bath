#!/usr/bin/env bash
source "$(dirname $0)/recipes"

if [[ "$@" != "" ]]; then
	recipes_names="$@"
else
	recipes_names="${!recipes[@]}"
fi

for k in $recipes_names; do
	if [[ -z ${recipes[$k]} ]]; then
		echo 'No such recipe: '$k
		continue
	fi

	./combine.sh ${recipes[$k]} > $k
	sed -i -e '/^$/d' -e 's/.*/0.0.0.0 \0/' $k
	domain_count=$(wc -l $k | cut -d\  -f 1)
	date="$(echo -n $(date -u +'%Y-%m-%d %H:%M:%S'))"
	hostfiles=$(echo -n ${recipes[$k]})
	sed -i -e '1s;^;#Created on '"$date"' UTC with '"$domain_count"' domains from recipe: '$k' = '"$hostfiles"'\n;' $k
	cat head - <<< "$(cat $k tail)" > $k
	wc -l $k
done
