# BATH (Block Advertisements and Tracking by Hosts) - Wash the dirt off your Internet traffic

## Kickstart

1. Run Linux and BASH. Make sure you have git, grep, sed, cut, wget and an Internet connection.
2. git clone https://bitbucket.org/block-ads-and-trackers/bath
3. cd bath
4. sudo ./run.sh
5. Enjoy your new /etc/hosts file. Your old /etc/hosts will be backed up in /etc, dated with the UTC time of it's replacement.

## What is this?

This is a tool that automates downloading and combining hosts files from different sources, making their installation and reverting to a previous hosts file easier as well. It supports your own blacklist, as well as whitelist, and allows you to exclude certain lists that it could otherwise download and use.

## How it works?

Sources are organized in separate directories and files. This program fetches every source, removes everything but the domains, sorts them and removes the duplicates, applies your blacklist and whitelist, adds 0.0.0.0 in front of every domain, prepends a head and appends a tail (which you could customize for yourself), then replaces your /etc/hosts, making a backup of your previous file.

Different recipes combine several sources to provide complete lists for different purposes. A recipe is what's ultimately installed as /etc/hosts.

## run.sh

This script combines everything - from the download to the installation (if run with a write access to /etc and /etc/hosts). The different steps are isolated in separate scripts, so you could run them on their own.

It takes recipes as arguments. If any are provided - it builds only those recipes and installs the first one, otherwise it builds all recipes and installs the default one.

For the installation to take place, you need to run this script as root or to have write access to /etc/hosts and /etc (for making backups). If you run it as a regular user - everything else will still execute, except the installation - which you could execute manually afterwards.

## prepare.sh

This script takes care of the different sources, preparing them for building into recipes. It takes source directories as arguments, preparing only the sources in those directories, or all sources otherwise.

What exactly takes place depends on the contents of the source directory. Every directory can contain its own prepare.sh script, which (if executable) is sourced in the main prepare.sh script.  It can also contain meta lists, which combine several of the already prepared sources.

### If you want to define your own sources

Just read the contents of those already provided. For example, the steven_black directory contains a single file (hosts_sources) with Steven Blacks's default hosts URL. The dbl_oisd_nl directory contains a prepare.sh file, which writes two URLs in two different files - for the two categories of sources.

The firebog_net and lightswitch05 directories contain examples for meta_lists files (the entries in which could be overwritten in the prepare.sh file, in which meta lists could also be defined). The prepare.sh files in those directories also could be used as examples for preparing sources in different ways.

### manually_downloaded

This directory is supposed to contain manually downloaded hosts files. It contains a prepare.sh file, which sanitizes and removes duplicates from every file present (if called with arguments - it does this for the files supplied in the arguments). Thus prepared, you can include them into recipes (the local prepare.sh script will be called automatically with no arguments by the main prepare.sh script).

## build.sh

This script takes recipes as arguments or builds all recipes in the 'recipes' file otherwise. It does not prepare the sources, so they should be prepared in advance. It writes the recipes in their own files in the root directory of the program (the same directory it resides) - ready to be installed.

### recipes

The recipes file contains an associative array with recipe names as keys and files with listed domains (one per line) as values, i. e. already prepared sources. Currently, a recipe can not include other recipes.

- The every_base recipe contains the minimal list from each source. This is the default recipe, which will be built and installed, if no recipe is specified to the respective scripts.
- The every_base_and_amp recipe contains the minimal list from each source, as well as the AMP hosts from lightswitch05.
- The medium recipe extends the previous two with other, considered as safe lists.
- The firebog_oisd_full recipe contains the full general list of domains, provided by firebog.net and oisd.nl.
- The every_full recipe contains the full general list by every source, excluding targeted subjects  and personal informational biases of their respective autor(s).

## install.sh

This script takes one recipe as an argument. It makes a backup of your existing /etc/hosts file with the current UTC date in the name of the backup in /etc, then installes the already built file with the recipe as /etc/hosts. If no recipe is provided - it uses the default one, defined in the 'recipes' file.

## revert.sh

This script restores the /etc/hosts file to the file before it was installed with install.sh. It makes a backup of the reverted /etc/hosts file in /etc with the UTC time it was reverted in the name. If there aren't any previous versions, which haven't been reverted  - the previously reverted /etc/hosts files are used instead.

## clean.sh

This script removes all files created in the process of preparing the separate sources, as well as the prepared sources themselves. It currently does not remove built recipes.

## exclude_list

Place here every source you wish to exclude from preparing a source directory - one per line. The prepare.sh script will check every URL of a hosts file against every line of this file and will skip it if it matches. It could be whole URL, part of a URL, or whatever grep pattern you wish.

## my_blacklist

Place the domain names you want to block in this file - one per line.

## my_whitelist

Place the domain names you want to exclude from blocking in this file - one per line. You could also use sed  patterns, which will exclude all domains that match the given pattern.

## head

The contents of this file are prepended to evry built recipe, i. e. every file that is or will be installed as /etc/hosts. You can add your own custom entries here, though I don't promise I'll not change this file in the future, which means you'll have to merge them into the changed file.

## tail

The contents of this file are appended to evry built recipe, i. e. every file that is or will be installed as /etc/hosts. This file is less likely to change than the head file, but I still can't sware it'll not change, so if you add any custom entries here - you'll have to merge them into the new file yourself in such case.

## sanitize.sh

This script takes care of removing all IP adresses in front of domains, all comments, blank lines, i. e. leaving only the naked domains from external hosts  lists. It is a part of the preparation process. You can also call it externally to test what it will remove.

## combine.sh

This script takes care of combining cleaned domain lists and applying your blacklist and whitelist. It is a part of the building process.

## check_exclude.sh

This script checks if a URL matches an entry in  the exclude_list file (see the information for exclude_list above). You can also call it manually for testing of your patterns.


# Questions and Suggestions

Write to: [bath@onionmail.org](mailto:bath@onionmail.org?subject=Bitbucket%20Contact)

