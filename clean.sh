#!/usr/bin/env bash

dirs="./*/"
if [[ "$@" != "" ]]; then
	dirs="$@"
fi

for src in $dirs; do
	if ! [[ -d $src ]]; then
		echo 'Not a directory: '$src'. Skipping...'
		continue
	fi

	echo 'Entering '$src'...'
	cd $src

	declare -A meta_lists=()

	if [[ -f ./meta_lists ]]; then
		source ./meta_lists
	fi

	for list_source in *_sources; do
		if [[ $list_source == '*_sources' ]]; then
			break
		fi
		list_name=$(sed 's/_sources$//g' <<< $list_source)
		if [[ "${list_name}" == "" ]]; then
			continue
		fi
		for ln in ${list_name}*; do
			if [[ ! -f ./prepare.sh && "$ln" == "$list_source" ]]; then
				echo 'Keeping '$ln', because prepare.sh is not present.'
				continue
			fi
			rm -v "$ln"
		done
	done

	for ml in ${!meta_lists[@]}; do
		if [[ -f "$ml" ]]; then
			rm -v "$ml"
		fi
	done

	echo 'Leaving '$src'...'
	cd ..
done
