#!/usr/bin/env bash

dirs="./*/"
if [[ "$@" != "" ]]; then
	dirs="$@"
fi

for src in $dirs; do
	if ! [[ -d $src ]]; then
		echo 'Not a directory: '$src'. Skipping...'
		continue
	fi

	echo 'Entering '$src'...'
	cd $src

	declare -A meta_lists=()

	if [[ -f ./meta_lists ]]; then
		source ./meta_lists
	fi

	if ! [[ -f ./prepare.sh ]]; then
		echo 'prepare.sh not found in '$src'.'
	elif ! [[ -x ./prepare.sh ]]; then
		echo $src'/prepare.sh is not executable. Skipping...'
	else
		source ./prepare.sh
	fi

	declare -A DOWNLOADED=()
	HASH=md5sum
	reuse=1

	for list_source in *_sources; do
		if [[ $list_source == '*_sources' ]]; then
			echo "no '*_sources' files present."
			break
		fi
		list_name=$(sed 's/_sources$//g' <<< $list_source)
		touch $list_name
		lists=$(cat $list_source)
		list_count_in_type=$(wc -l <<< $lists)
		echo 'Lists in '$list_name': '$list_count_in_type
		index=0
		fresh=0
		reused=0
		excluded=0
		failed=0
		for list in $lists; do
			index=$(($index+1))
			list_filename=$list_name'_'$index
			../check_exclude.sh $list
			if [[ "$?" == "1" ]]; then
				if [[ -f "$list_filename" ]]; then
					rm -f $list_filename
				fi
				excluded=$(($excluded+1))
				continue
			fi
			touch $list_filename
			list_hash=$($HASH <<< $list | cut -d\  -f 1)
			if [[ $reuse != 0 && "${DOWNLOADED[$list_hash]}" != "" ]]; then
				echo 'Taking '$list_filename' from '${DOWNLOADED[$list_hash]}' for '$list
				cp ${DOWNLOADED[$list_hash]}  $list_filename
				if [[ "$?" == "0" ]]; then
					reused=$(($reused+1))
					continue
				fi
			fi
			echo 'Downloading '$list' into '$list_filename
			wget -q $list -O $list_filename
			if [[ "$?" != "0" ]]; then
				echo 'Error downloading '$list
				failed=$(($failed+1))
				continue
			fi
			fresh=$(($fresh+1))
			DOWNLOADED[$list_hash]=$list_filename
			echo 'Lines downloaded: '$(wc -l $list_filename)
			../sanitize.sh $list_filename
			echo 'Lines written after sanitizing: '$(wc -l $list_filename)
		done
		echo $list_name' finished: New: '$fresh', Reused: '$reused' (Total: '$(($reused+$fresh))$([ $(($reused+$fresh)) != $list_count_in_type ] && echo -n ', but should have been '$list_count_in_type' ('$(printf '%+d' $(($list_count_in_type-($reused+$fresh))))') - Excluded: '$excluded',  Failed: '$failed)').'
		cat ${list_name}'_'* | sort -u > $list_name
		echo 'Lines written: '$(wc -l $list_name)
	done

	for ml in ${!meta_lists[@]}; do
		cat ${meta_lists[$ml]} | sort -u > $ml
		tl=(${meta_lists[$ml]})
		for l in ${tl[@]::(${#tl[@]}-1)}; do
			echo -n $l' + '
		done
		echo ${tl[-1]}' = '$(wc -l $ml)
	done

	echo 'Leaving '$src'...'
	cd ..
done
