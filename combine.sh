#!/usr/bin/env bash

data=$(cat $@ my_blacklist | sort -u)
whitelist=$(cat my_whitelist)

for entry in $whitelist; do
	data=$(sed '/.*'$entry'.*/d' <<< $data)
done

cat <<< $data
