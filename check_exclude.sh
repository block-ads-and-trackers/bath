#!/usr/bin/env bash

url=""
if [[ "$1" != "" ]]; then
	url="$1"
else
	echo 'No URL provided.'
	exit 3
fi

exclude_list="$(dirname $0)/exclude_list"
if [[ "$2" != "0" ]]; then
	exclude_file="$2"
fi
if ! [[ -f "$exclude_list" ]]; then
	echo ' No such file: '$exclude_list
	exit 4
fi
if ! [[ -r "$exclude_list" ]]; then
	echo ' File not readable: '$exclude_list
	exit 5
fi

grep_output=$(grep -q -s -f $exclude_list <<< $url 2>&1)
grep_result="$?"
if [[ "$grep_result" == "0" ]]; then
	echo 'One of the grep patterns in '$(realpath $exclude_list)' matches the URL '$url'. Skipping...'
	exit 1
elif [[ "$grep_result" == "2" ]]; then
	if [[ "$grep_output" != "" ]]; then
		file_name=$(cut -d: -f2 <<< $grep_output)
		line_number=$(cut -d: -f3 <<< $grep_output)
		error_message=$(cut -d: -f4- <<< $grep_output)
		line_contents=$(sed -n "${line_number}p" $file_name)
		echo 'You have an error in one of the grep patterns in '$(realpath $file_name)': "'$error_message'".'
		echo 'The line is #'$line_number': "'$line_contents'"'
		echo 'Fix it or remove it to be able to exclude lists by patterns in URL.'
		exit 2
	fi
fi

exit 0
