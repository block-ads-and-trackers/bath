#!/usr/bin/env bash

echo 'Downloading https://v.firebog.net/hosts/lists.php?type=tick into tick_sources'
wget -q https://v.firebog.net/hosts/lists.php?type=tick -O tick_sources
echo 'Downloading https://v.firebog.net/hosts/lists.php?type=nocross into nocross_sources'
wget -q https://v.firebog.net/hosts/lists.php?type=nocross -O nocross_sources
echo 'Downloading https://v.firebog.net/hosts/lists.php?type=all into all_sources'
wget -q https://v.firebog.net/hosts/lists.php?type=all -O all_sources
echo 'Downloading https://v.firebog.net/hosts/csv.txt into csv_data'
wget -q https://v.firebog.net/hosts/csv.txt -O csv_data
echo 'Extracting URLs from csv_data into csv_sources'
sed 's/^.*\?"\(http.*\?\)".*\?$/\1/g' csv_data > csv_sources

